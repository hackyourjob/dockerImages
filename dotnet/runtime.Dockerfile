# https://hub.docker.com/_/microsoft-dotnet-aspnet/
FROM mcr.microsoft.com/dotnet/aspnet:6.0.1-bullseye-slim-amd64
WORKDIR /app
EXPOSE 8080
ENV SERVER_HOST "http://0.0.0.0:8080"
