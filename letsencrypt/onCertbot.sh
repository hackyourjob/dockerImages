#!/bin/bash

set -e

docker compose run --rm --entrypoint $1 certbot ${@:2}

docker compose kill -s HUP entrypoint
