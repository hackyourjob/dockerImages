# jwilder/nginx-proxy
FROM nginx:1.21.6

ENV DOCKER_HOST unix:///tmp/docker.sock

VOLUME ["/etc/nginx/certs", "/etc/nginx/dhparam"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["forego", "start", "-r"]

# Install wget and install/updates certificates
RUN apt-get update \
 && apt-get install -y -q --no-install-recommends \
    ca-certificates \
    wget \
    unzip \
 && apt-get clean \
 && rm -r /var/lib/apt/lists/*

COPY ./proxy.conf ./nginx.conf /etc/nginx/

# Install Forego
ADD https://github.com/jwilder/forego/releases/download/v0.16.1/forego /usr/local/bin/forego
RUN chmod u+x /usr/local/bin/forego

ENV DOCKER_GEN_VERSION 0.7.6

RUN wget https://github.com/jwilder/docker-gen/releases/download/$DOCKER_GEN_VERSION/docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz \
 && tar -C /usr/local/bin -xvzf docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz \
 && rm /docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz

ENV NGINX_PROXY 35df872705bf18def497668ee496ae08a75bfa2f

RUN mkdir /tmp/proxy \
 && cd /tmp/proxy \
 && wget https://github.com/nginx-proxy/nginx-proxy/archive/$NGINX_PROXY.zip \
 && unzip -a $NGINX_PROXY.zip \
 && rm $NGINX_PROXY.zip \
 && cd nginx-proxy-$NGINX_PROXY/ \
 && mkdir /app \
 && cp Procfile /app/ \
 && cp dhparam.pem.default /app/ \
 && cp docker-entrypoint.sh /app/ \
 && cp generate-dhparam.sh /app/ \
 && cp network_internal.conf /etc/nginx/network_internal.conf \
 && cp nginx.tmpl /app/ \
 && sed -i 's/access.log vhost;/access.log matomo;/' /app/nginx.tmpl \
 && cd / \
 && rm -Rf /tmp/proxy

WORKDIR /app/
