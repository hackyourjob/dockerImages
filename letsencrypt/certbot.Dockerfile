FROM certbot/certbot:v1.22.0
WORKDIR /app
ENTRYPOINT ["/usr/local/bin/certbot"]
CMD ["--version"]

RUN apk add --no-cache --virtual .build-deps gcc openssl-dev musl-dev \
&& pip install --no-cache-dir certbot-dns-ovh dns-lexicon \
&& apk del .build-deps

COPY ./scripts /app
RUN chmod +x -R *.sh gandi/auth.py gandi/clean.py
