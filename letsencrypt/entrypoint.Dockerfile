FROM alpine as key-generator
RUN apk add --no-cache curl
RUN curl https://ssl-config.mozilla.org/ffdhe2048.txt > /ffdhe2048.txt


FROM haproxy:2.5.1 as base
EXPOSE 80 443 8080 8081
COPY ./haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg
COPY --from=key-generator /ffdhe2048.txt /usr/local/etc/haproxy/dhparam


FROM base as long-timeout
USER root
RUN sed -i 's/timeout client  50000;/timeout client  600000;/' /usr/local/etc/haproxy/haproxy.cfg \
 && sed -i 's/timeout server  50000;/timeout server  600000;/' /usr/local/etc/haproxy/haproxy.cfg
USER haproxy


FROM base as without-h2
USER root
RUN sed -i 's/ alpn h2,http\/1\.1//' /usr/local/etc/haproxy/haproxy.cfg
USER haproxy
