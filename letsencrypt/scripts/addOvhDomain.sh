#!/bin/sh

set -e

/usr/local/bin/certbot \
    certonly \
    --server https://acme-v02.api.letsencrypt.org/directory \
    --dns-ovh \
    --dns-ovh-credentials /app/ovh/config \
    --manual-public-ip-logging-ok \
    --preferred-challenges dns \
    -d $@

./exportCertificates.sh
