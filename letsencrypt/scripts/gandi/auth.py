#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import time
from config import *
from api import *

config = Config(livedns_apikey)
config.checkConfig()

print("Request " + config.certbot_domain + " => " + config.certbot_validation)

api = config.buildApi()

print("Append " + config.getToken() + " ==> " + api.apiUri)

result = api.append(config.getToken())
if result:
    print("all good, entry created")
else:
    print("failed")
    exit(1)

time.sleep(60)