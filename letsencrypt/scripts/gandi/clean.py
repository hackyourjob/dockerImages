#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from config import *
from api import *

config = Config(livedns_apikey)
config.checkConfig()

print("Request " + config.certbot_domain + " => " + config.certbot_validation)

api = config.buildApi()

print("Clean " + api.apiUri)

result = api.remove()
if result:
    print("all good, entry created")
else:
    print("failed")
    exit(1)
