#!/bin/sh

set -e

/usr/local/bin/certbot \
    certonly \
    --server https://acme-v02.api.letsencrypt.org/directory \
    --manual \
    --manual-auth-hook /app/gandi/auth.py \
    --manual-cleanup-hook /app/gandi/clean.py \
    --manual-public-ip-logging-ok \
    --preferred-challenges dns \
    -d $@

./exportCertificates.sh
