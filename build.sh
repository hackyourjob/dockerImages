#!/usr/bin/env bash

set -eu

VERSION=":$1"
PREFIX="registry.gitlab.com/hackyourjob/dockerimages"
INFO_COLOR="\033[1;34m"
CLEAR_COLOR="\033[0m"

declare -a images_to_push

function log_info {
    local message=$1
    echo -e "${INFO_COLOR}${message}${CLEAR_COLOR}"
}

function build {
    local image_name=$1
    local extra_args=${@:2}
    local full_image_name=$PREFIX/$image_name$VERSION

    log_info "build $image_name"
    docker build \
       --cache-from $PREFIX/$image_name:latest \
       --cache-from $full_image_name \
       --build-arg BUILDKIT_INLINE_CACHE=1 \
       -t $full_image_name . \
       $extra_args

    images_to_push+=($full_image_name)
}

function push_all {
    for image in "${images_to_push[@]}"; do
        log_info "push $image"
        docker push "$image"
    done
}

cd databases
build databases/adminer -f adminer.Dockerfile

cd ../docker
build docker/compose -f compose.Dockerfile

cd ../dotnet
build dotnet/build -f build.Dockerfile --target base
build dotnet/build_node -f build.Dockerfile --target node

build dotnet/runtime -f runtime.Dockerfile

cd ../letsencrypt
build letsencrypt/certbot -f certbot.Dockerfile
build letsencrypt/discovery -f discovery.Dockerfile
build letsencrypt/entrypoint -f entrypoint.Dockerfile --target base
build letsencrypt/entrypoint-long-timeout -f entrypoint.Dockerfile --target long-timeout
build letsencrypt/entrypoint-without-h2 -f entrypoint.Dockerfile --target without-h2

cd ../nodejs
build nodejs/runtime --target runner_base
build nodejs/chrome --target chrome

cd ../php
build php/base-73 -f 73.Dockerfile --target base-73
build php/builder-73 -f 73.Dockerfile --target builder-73
build php/runner-73 -f 73.Dockerfile --target runner-73

build php/base-74 -f 74.Dockerfile --target base-74
build php/builder-74 -f 74.Dockerfile --target builder-74
build php/runner-74 -f 74.Dockerfile --target runner-74

cd ../proxy
build proxy


push_all
