FROM docker:20.10.12

RUN apk add --no-cache make git curl bash \
 && mkdir -p ~/.docker/cli-plugins/ \
 && curl -SL https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose \
 && chmod +x ~/.docker/cli-plugins/docker-compose

ENV DOCKER_BUILDKIT=1
ENV COMPOSE_DOCKER_CLI_BUILD=1
